import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const fetchBlog = async (id: number) => {
	const options = {
		headers: {
			Authorization: `Bearer ${process.env.STRAPI_API_TOKEN}`,
		},
	}

	try {
		const res = await fetch(
			`${process.env.STRAPI_API_URL}/api/blogs/${id}?populate=*`,
			options
		)

		const response = await res.json()

		return response
	} catch (error) {
		console.log(error)
	}
}

const BlogDetail = async ({ params }: any) => {
	console.log(params.id)

	const blog = (await fetchBlog(params.id)) as any

	console.log(blog)

	return (
		<div className='max-w-3xl mx-auto p-4'>
			<Link href={'/'} className='text-blue-500 uppercase'>
				Back
			</Link>
			<div className='relative w-full h-96 overflow-hidden rounded-lg mt-5'>
				{/* <Image
					fill
					src={
						process.env.STRAPI_API_URL +
						blog.attributes.img.data.attributes.url
					}
					alt='example'
					className='rounded-t-lg object-cover'
					priority
					sizes='(max-width: 640px) 100vw, (max-width: 768px) 50vw, 33vw'
				/> */}
			</div>
			<div className='mt-4'>
				<h1 className='text-3xl font-semibold'>
					{blog.data.attributes.title}
				</h1>
				<p className='text-gray-600 mt-2'>
					{blog.data.attributes.description[0].children[0].text}
				</p>
			</div>
			<div className='mt-4 flex items-center text-gray-400'>
				<span className='text-sm'>Published on {Date.now()}</span>
			</div>
		</div>
	)
}

export default BlogDetail

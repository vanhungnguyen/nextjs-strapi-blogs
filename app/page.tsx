import Blogs from '@/components/Blogs'
import Categories from '@/components/Categories'

const fetchCategories = async () => {
	const options = {
		headers: {
			Authorization: `Bearer ${process.env.STRAPI_API_TOKEN}`,
		},
	}

	try {
		const res = await fetch(
			`${process.env.STRAPI_API_URL}/api/categories`,
			options
		)

		const response = await res.json()

		return response
	} catch (error) {
		console.log(error)
	}
}

const fetchBlogs = async () => {
	const options = {
		headers: {
			Authorization: `Bearer ${process.env.STRAPI_API_TOKEN}`,
		},
	}

	try {
		const res = await fetch(
			`${process.env.STRAPI_API_URL}/api/blogs?populate=*`,
			options
		)

		const response = await res.json()

		return response
	} catch (error) {
		console.log(error)
	}
}

export default async function Home() {
	const categories = await fetchCategories()
	const blogs = await fetchBlogs()

	return (
		<div>
			<Categories categories={categories} />
			<Blogs blogs={blogs} />
		</div>
	)
}

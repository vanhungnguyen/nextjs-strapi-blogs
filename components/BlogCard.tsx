import React from 'react'
import Link from 'next/link'
import Image from 'next/image'

const BlogCard = ({ blog }: any) => {
	const truncateBlogDesc =
		blog.attributes.description[0].children[0].text.length > 80
			? blog.attributes.description[0].children[0].text.substring(0, 80) +
			  '...'
			: blog.attributes.description[0].children[0].text

	return (
		<div className='rounded-lg shadow-md p-4 mb-4 overflow-hidden border border-gray-600 cursor-pointer'>
			<Link href={`/blog/${blog.id}`}>
				<div className='relative w-full h-20'>
					<Image
						fill
						src={
							process.env.STRAPI_API_URL +
							blog.attributes.img.data.attributes.url
						}
						alt='example'
						className='rounded-t-lg object-cover'
						priority
						sizes='(max-width: 640px) 100vw, (max-width: 768px) 50vw, 33vw'
					/>
				</div>
				<div className='p-2'>
					<h2 className='text-xl font-semibold mb-2'>
						{blog?.attributes?.title}
					</h2>
					<p className='text-gray-600'>{truncateBlogDesc}</p>
				</div>
			</Link>
		</div>
	)
}

export default BlogCard

'use client'
import React from 'react'

const Category = ({ cat }: any) => {
	return (
		<div
			onClick={() => {
				console.log('clicked')
			}}
			className='bg-gray-400 p-4 rounded-lg shadow-md cursor-pointer'
		>
			{cat?.attributes?.title}
		</div>
	)
}

export default Category
